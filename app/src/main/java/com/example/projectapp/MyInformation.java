package com.example.projectapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MyInformation extends AppCompatActivity {
    public static final String DB_NAME = "User.db";
    DBHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_information);

        dbHelper = new DBHelper(getApplicationContext(), DB_NAME, null, 1 );
    }

    /*public void makeToast(String str)
    {
        Toast.makeText(
                MyInformation.this,
                str,
                Toast.LENGTH_SHORT)
                .show();
    }*/

    public void gamePage(View view)
    {
        Intent intent = new Intent(this, game.class);
        startActivity(intent);

    }
}